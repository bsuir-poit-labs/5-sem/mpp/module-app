﻿using System;
using System.IO;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using UI_Attributes;

namespace Module_App
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const string PluginsPath = "./plugins";

        public MainWindow()
        {
            InitializeComponent();
            LoadPlugins(PluginsPath);
        }

        private void LoadPlugins(string pluginsPath)
        {
            var pluginsInfo = GetPluginsInfo(pluginsPath);
            foreach (FileInfo fileInfo in pluginsInfo)
            {
                var assembly = Assembly.LoadFile(fileInfo.FullName);
                HandlePluginAssembly(assembly);
            }
        }

        private void HandlePluginAssembly(Assembly assembly)
        {
            var types = assembly.GetTypes();
            foreach (var type in types)
            {
                if (type.BaseType == typeof(UserControl))
                {
                    HandleUserControl(type);
                }
            }
        }

        private void HandleUserControl(Type type)
        {
            var attributes = type.GetCustomAttributes(false);
            foreach (var attribute in attributes)
            {
                if (attribute is EmbedAttribute embedAttribute)
                {
                    HandleAttribute(embedAttribute, type);
                }
            }
        }

        private void HandleAttribute(EmbedAttribute attribute, Type type)
        {
            var control = (UserControl) type.GetConstructor(new Type[0])?.Invoke(null);
            if (!attribute.IsEmbedInTab) 
                return;
            
            var tabItem = new TabItem()
            {
                Header = attribute.Header,
                Content = new Grid()
                {
                    Background = new SolidColorBrush(attribute.Color),
                    Children = {control ?? throw new InvalidOperationException()}
                }
            };

            TabControl.Items.Add(tabItem);
        }

        private FileInfo[] GetPluginsInfo(string pluginsPath)
        {
            if (!Directory.Exists(pluginsPath))
                throw new DirectoryNotFoundException("Plugins directory doesn't exist");

            var directoryInfo = new DirectoryInfo(pluginsPath);
            return directoryInfo.GetFiles();
        }
    }
}
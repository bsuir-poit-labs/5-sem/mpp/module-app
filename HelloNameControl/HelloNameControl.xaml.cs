using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using UI_Attributes;

namespace HelloNameControl
{
    [Embed("Greetings", true, 204, 204, 255)]
    public partial class UserControl1 : UserControl
    {
        public UserControl1()
        {
            InitializeComponent();
        }

        private void BtnAdd_OnClick(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Hello, " + TbName.Text + "!", "Greetings");
        }
    }
}
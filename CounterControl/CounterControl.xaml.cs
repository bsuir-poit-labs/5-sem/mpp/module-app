using System.Windows;
using System.Windows.Controls;
using UI_Attributes;

namespace CounterControl
{
    [Embed("Counter", true, 255, 204, 204)]
    public partial class UserControl1 : UserControl
    {
        public int Count { get; set; }
        
        public UserControl1()
        {
            InitializeComponent();
            Count = 0;
        }

        private void BtnAdd_OnClick(object sender, RoutedEventArgs e)
        {
            Count++;
            LbCount.Content = Count;
        }
    }
}
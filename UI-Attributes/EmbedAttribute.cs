﻿using System;
using System.Windows.Media;

namespace UI_Attributes
{
    public class EmbedAttribute : Attribute
    {
        public bool IsEmbedInTab { get; set; }
        
        public string Header { get; set; }

        public Color Color { get; set; }

        public EmbedAttribute(string header, bool isEmbedInTab, byte r, byte g, byte b)
        {
            Header = header;
            IsEmbedInTab = isEmbedInTab;
            Color = Color.FromArgb(byte.MaxValue, r, g, b);
        }
    }
}
